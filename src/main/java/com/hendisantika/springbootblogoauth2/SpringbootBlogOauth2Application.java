package com.hendisantika.springbootblogoauth2;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringbootBlogOauth2Application {

	public static void main(String[] args) {
		SpringApplication.run(SpringbootBlogOauth2Application.class, args);
	}

}
