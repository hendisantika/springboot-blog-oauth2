package com.hendisantika.springbootblogoauth2.model;

import java.io.Serializable;

/**
 * Created by IntelliJ IDEA.
 * Project : springboot-blog-oauth2
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 08/02/20
 * Time: 06.08
 */
public class Welcome implements Serializable {

    private static final String GREETINGS_FORMAT = "Welcome %s!";

    public String greetings;

    public Welcome() {
    }

    public Welcome(String who) {
        this.greetings = String.format(GREETINGS_FORMAT, who);
    }
}