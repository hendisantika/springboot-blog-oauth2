package com.hendisantika.springbootblogoauth2.repository;

import com.hendisantika.springbootblogoauth2.model.AppUser;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Created by IntelliJ IDEA.
 * Project : springboot-blog-oauth2
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 08/02/20
 * Time: 06.09
 */
public interface AppUserRepository extends JpaRepository<AppUser, String> {

}